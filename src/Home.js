import React from 'react';
import Titleimage from './components/Title/Titleimage';
import OurApproach from './components/ourapproach/OurApproach';
import OurCourses from './components/OurCourses/OurCourses';
import Quotes from './components/Quotes/Quotes';
import Partner from './components/Hiringpartner/Partner';
import Article from './components/Article/Article';

const Home = () => {
    return (
        <div>
            <Titleimage />
            <OurApproach />
            <OurCourses />
            <Quotes />
            <Partner/>
            <Article />
        </div>
    )
}

export default Home
