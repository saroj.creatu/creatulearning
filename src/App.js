import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/Navbar/Navbar';
import HomeRouting from './Router/HomeRouting';
import Footer from './components/Footer/Footer';

function App() {
  return (
    <div className='learning'>
      <Navbar />
      <HomeRouting />
      <Footer />
    </div>
  );
}

export default App;
