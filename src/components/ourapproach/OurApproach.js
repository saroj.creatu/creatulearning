import React from 'react';
import './ourApproach.css';

const OurApproach = () => {
    return (
        <div className='approach-main'>
            <img src="images/two.jpg" alt="approach" className='approach-img' />
        </div>
    )
}

export default OurApproach
