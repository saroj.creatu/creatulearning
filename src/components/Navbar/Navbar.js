import React from 'react';
import FirstHeader from './FirstHeader/FirstHeader';
import SecondHeader from './SecondHeader/SecondHeader';
import './Navbar.css'

const Navbar = () => {
    return (
        <div className='main-navbar'>
            <FirstHeader />
            <SecondHeader />
        </div>
    )
}

export default Navbar
