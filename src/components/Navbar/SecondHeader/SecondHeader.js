import React from 'react'
import { NavLink } from 'react-router-dom'
import './SecondHeader.css'

const SecondHeader = () => {
    return (
        <div className='secondHeader-main'>
            <div className='container' >
                <div className='row' > 
                    <div className='col-md-3'>
                        <div className='sh-topic'>
                            <NavLink to="/offers" className="sec-head-links">OFFERS</NavLink>
                        </div>
                    </div>
                    <div className='col-md-3'>
                        <div className='sh-topic'>
                            <NavLink to="/career" className="sec-head-links">CAREER</NavLink>
                        </div>
                    </div>
                    <div className='col-md-3'>
                        <div className='sh-topic'>
                            <NavLink to="/blog" className="sec-head-links">BLOG</NavLink>
                        </div>
                    </div>
                    <div className='col-md-3'>
                        <div className='sh-topic'>
                            <NavLink to="/verifycerficate" className="sec-head-links">VERIFY CERTIFICATE</NavLink>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SecondHeader
