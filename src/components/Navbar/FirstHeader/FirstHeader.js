import React from 'react';
import { NavLink } from 'react-router-dom';
import './FirstHeader.css';

const FirstHeader = () => {
    return (
        <div className=' fh-main'>
            <div className='container'>
                <div className='row'>
                        <div className='col-md-3 col-sm-4' >
                                <div className='align-main'>
                                    <i class="fas fa-align-justify align"></i> <span className='align-part'>All Courses</span>
                                </div>
                        </div>
                        <div className='col-md-6 col-sm-4'>
                            <div className='firstheader-logo-main'>
                                <NavLink to="/"><img src="images/creatulogo.png" alt="learning-logo" id="home" className='fh-logo' /></NavLink>
                            </div>
                        </div>
                        <div className='col-md-3 col-sm-4' >
                            <div className='ul-li-main'>
                                <ul className='ul-main' >
                                    <li className='con-li' >Call Us: 01-5192060</li>
                                    <li className='con-li'>+977 9851087672</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    )
}

export default FirstHeader
