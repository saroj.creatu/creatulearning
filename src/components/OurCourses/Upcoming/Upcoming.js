import React from 'react';
import UpcomingList from './UpcomingList';
import UpcomingDesign from './UpcomingDesign';
import './Upcoming.css'

const Upcoming = () => {
    return (
        <div>
            <div className='row' >
                {UpcomingList.map( (Elem)=> {
                    return(
                        <div className='col-md-3' >
                            <UpcomingDesign 
                                name = {Elem.name}
                                image = {Elem.image}
                            />
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default Upcoming
