import React from 'react';
import './Upcoming.css'

const UpcomingDesign = (props) => {
    return (
        <div className='main-trend'>
            <div className='trend-main-img'>
                <img src={props.image} alt="trend" className="trend-img" />
            </div>
            <h3>{props.name}</h3>
            <p><i class="far fa-clock"></i> Duration : 3 months</p>
            <p>Coming Soon .....</p>
            <button className='trend-btn' ><i class="fas fa-clipboard-check"></i> Enroll Now</button>
        </div>
    )
}

export default UpcomingDesign
