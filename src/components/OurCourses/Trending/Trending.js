import React from 'react';
import TrendingList from './TrendingList';
import TrendingDesign from './TrendingDesign';

const Trending = () => {
    return (
        <div className='container' >
            <div className='row' >
                {TrendingList.map( (Elem)=> {
                    return(
                        <div className='col-md-3' >
                            <TrendingDesign 
                                name = {Elem.name}
                                image = {Elem.image}
                            />
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default Trending
