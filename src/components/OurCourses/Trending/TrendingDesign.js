import React from 'react';
import './Trending.css'

const TrendingDesign = (props) => {
    return (
        <div className='main-trend'>
            <div className='trend-main-img'>
                <img src={props.image} alt="trend" className="trend-img" />
            </div>
            <h3>{props.name}</h3>
            <p><i class="far fa-clock"></i> Duration : 3 months</p>
            <button className='trend-btn' ><i class="fas fa-clipboard-check"></i> Enroll Now</button>
        </div>
    )
}

export default TrendingDesign
