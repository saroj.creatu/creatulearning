import React, {useState} from 'react';
import { NavLink } from 'react-router-dom';
import './OurCourses.css'
import Trending from './Trending/Trending';
import Upcoming from './Upcoming/Upcoming';

const OurCourses = () => {

    const [state, setState] = useState(true);

    return (
        <div className='courses-main'>
        <p className='courses-icon'><i class="fas fa-book-reader"></i></p>
            <h2 className='coureses-topic'>- OUR COURSES -</h2>
            <div className='container' >
                <div className='course-main-btn' >
                    <button className='course-btn'  onClick={()=>setState(true)}><NavLink activeClassName className='active_link' to="/" >Trending Courses</NavLink></button>   
                    <button className='course-btn'  onClick={()=>setState(false)} ><NavLink   className='active_link' to="/">Upcoming Courses</NavLink></button>

                    {(state)? <Trending/>:<Upcoming />}
                </div>
            </div>
            <NavLink to="/allcourses" className='active_link'><button className='explore-btn' >Explore All Courses</button></NavLink>
        </div>
    )
}

export default OurCourses
