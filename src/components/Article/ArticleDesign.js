import React from 'react'
import { NavLink } from 'react-router-dom'

const ArticleDesign = (props) => {
    const handlerbtn = ()=> {

    }
    return (
        <div className='article-cart-main'>
            <div className='article-img-main'>
                <img src={props.image} alt="" className="article-img" />
            </div>
            <h3 className='article-cart-topic'>{props.topic}</h3>
            <p className='cart-text'> {props.text} </p>
            <NavLink to="" className="article-link"><button onClick={handlerbtn} className='cart-btn'>Read More</button></NavLink>
        </div>
    )
}

export default ArticleDesign
