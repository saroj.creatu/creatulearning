const ArticleList = [
    {
        id: 1,
        image : "images/articleone.png",
        topic : "Why Learn Python in 2022?",
        text : "Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. Its high-level built-in data structures, combined with dynamic typing and dynamic binding, make it ve..."
    },
    {
        id: 2,
        image : "images/articletwo.jpg",
        topic : "Why learn Digital Marketing in 2022?",
        text : "In this modern era, people have grown extremely dependent on online platforms for various uses. It is no wonder why digital marketing has massively grown. It is a technical and creative field so.."
    },
    {
        id: 3,
        image : "images/articlethree.png",
        topic : "Is your website mobile?",
        text : "This is the era of mobile technology devices. Every single person that is either tech-savvy or normal people every single has been dependent on the mobile device. Mobile devices today have become.. "
    }
]
export default ArticleList;
