import React from "react";
import { NavLink } from "react-router-dom";
import "./Article.css";
import ArticleDesign from "./ArticleDesign";
import ArticleList from "./ArticleList";

const Article = () => {
  return (
    <div className="article-main">
      <div className="container">
          <p className="article-icon">
            <i class="fas fa-book-open"></i>
          </p>
          <h2 className="main-topic">- Our Article -</h2>
          <div className="article-second-main">
          <div className="row article-cart-map">
            {ArticleList.map((curlElem) => {
              return (
                <div className="col-md-4">
                  <ArticleDesign
                    id={curlElem.id}
                    image={curlElem.image}
                    topic={curlElem.topic}
                    text={curlElem.text.substring(0,300)}
                  />
                </div>
              );
            })}
          </div>
          </div>
          <NavLink to="">
            <button className="article-explore">Explore More</button>
          </NavLink>
      </div>
    </div>
  );
};

export default Article;
