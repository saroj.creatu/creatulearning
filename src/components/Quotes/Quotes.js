import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import './Quotes.css';

const Quotes = () => {
  var settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <div className="quotes-main" >
    <p className="quotes-icon"><i class="fas fa-user-graduate"></i></p>
        <h2 className="quotes-topic">-  What Our Students Say  - </h2>
      <div className="container" >
      <Slider {...settings}>
            <div className="quotes-content-main">
                <h3 className="quotes-content">"    Lorem ipsum dolor sit amet consectetur adipisicing elit.<br /> Aut ullam eligendi blanditiis.    "</h3>
                <div className="content-main">
                    <h4 className="content">- Rakesh Basnet</h4>
                    <h4 className="content" >Course Enrolled : Laravel</h4>
                </div>
            </div>
        <div className="quotes-content-main">
          <h3 className="quotes-content">"    Lorem ipsum dolor sit amet consectetur adipisicing elit.<br /> Aut ullam eligendi blanditiis.    "</h3>
          <div className="content-main">
              <h4 className="content">- Bibisha dahal</h4>
              <h4 className="content">Course Enrolled : Java</h4>
          </div>
          </div>
        <div className="quotes-content-main">
          <h3 className="quotes-content">"    Lorem ipsum dolor sit amet consectetur adipisicing elit.<br /> Aut ullam eligendi blanditiis.    "</h3>
          <div className="content-main">
              <h4 className="content">- Subash Pandey</h4>
              <h4 className="content">Course Enrolled : React</h4>
          </div>
        </div>
        <div className="quotes-content-main">
          <h3 className="quotes-content">"    Lorem ipsum dolor sit amet consectetur adipisicing elit.<br /> Aut ullam eligendi blanditiis.    "</h3>
          <div className="content-main">
              <h4 className="content">- Hisha Maharjan</h4>
              <h4 className="content">Course Enrolled : Node Js</h4>          
            </div>
        </div>
        <div className="quotes-content-main">
          <h3 className="quotes-content">"    Lorem ipsum dolor sit amet consectetur adipisicing elit.<br /> Aut ullam eligendi blanditiis.    "</h3>
          <div className="content-main">
              <h4 className="content">- Yubraj Lama</h4>
              <h4 className="content">Course Enrolled : SEO</h4>
          </div>
        </div>
        <div className="quotes-content-main">
          <h3 className="quotes-content">"    Lorem ipsum dolor sit amet consectetur adipisicing elit.<br /> Aut ullam eligendi blanditiis.    "</h3>
          <div className="content-main">
              <h4 className="content">- Barsha Pandey</h4>
              <h4 className="content">Course Enrolled : Graphic Design</h4>
          </div>
        </div>
      </Slider>
      </div>
    </div>
  );
};

export default Quotes;
