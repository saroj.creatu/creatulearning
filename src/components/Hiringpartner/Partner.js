import React from 'react';
import './Partner.css'

const Partner = () => {
    return (
        <div className='partner-main'>
        <p className='partner-icon'><i class="far fa-handshake"></i></p>
        <h2 className='partner-topic'>- Our Hiring Partner -</h2>
            <div className='container'>
                <div className='row'>
                    <div className='col-md-2 col-sm-4'>
                        <div className='partner-logo-main'>
                            <img src="images/bhawani.jpg" alt="img" className='partner-logo' />
                        </div>
                    </div>
                    <div className='col-md-2 col-sm-4'>
                        <div className='partner-logo-main'>
                            <img src="images/bhawani.jpg" alt="img" className='partner-logo' />
                        </div>
                    </div>
                    <div className='col-md-2 col-sm-4'>
                        <div className='partner-logo-main'>
                            <img src="images/bhawani.jpg" alt="img" className='partner-logo' />
                        </div>
                    </div>
                    <div className='col-md-2 col-sm-4'>
                        <div className='partner-logo-main'>
                            <img src="images/bhawani.jpg" alt="img" className='partner-logo' />
                        </div>
                    </div>
                    <div className='col-md-2 col-sm-4'>
                        <div className='partner-logo-main'>
                            <img src="images/bhawani.jpg" alt="img" className='partner-logo' />
                        </div>
                    </div>
                    <div className='col-md-2 col-sm-4'>
                        <div className='partner-logo-main'>
                            <img src="images/bhawani.jpg" alt="img" className='partner-logo' />
                        </div>
                    </div>
                </div>
                <button className='btn-partner'>Explore All</button>
            </div>
        </div>
    )
}

export default Partner
