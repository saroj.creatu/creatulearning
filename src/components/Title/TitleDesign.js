import React from 'react'
import { NavLink } from 'react-bootstrap';



const TitleDesign = (props) => {
    return (
        <div className='title-main'>
            <NavLink to="" className='title-link' >
                <div className='card-title'>
                    <div className='title-image-main'>
                        <img src={props.image} alt="source" className='title-image' />
                    </div>
                    <p className='title-name'>{props.name}</p>
                </div>
                <div className='title-btn-main'>
                    <button className='title-btn'>Learn More</button>
                </div> 
            </NavLink>
        </div>
    )
}

export default TitleDesign
