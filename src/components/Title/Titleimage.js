import React from 'react';
import TitleList from './TitleList';
import TitleDesign from './TitleDesign';
import './Title.css'

const Titleimage = () => {
    return (
        <div className='main-title-card'>
            <div className='container'>
                <div className='row' >
                    {TitleList.map( (curlElem)=> {
                        return(
                            <div className='col-md-4'>
                                <TitleDesign 
                                    name = {curlElem.name}
                                    image = {curlElem.image}
                                />
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}

export default Titleimage
