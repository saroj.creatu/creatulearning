import React from 'react';
import './Footer.css'

const Footer = () => {
    return (
        <div className='main-footer'>
            <div className='container'>
                <div className='row'>
                    <div className='col-md-3'>Company</div>
                    <div className='col-md-3'>Related Links</div>
                    <div className='col-md-3'>Contact US</div>
                    <div className='col-md-3'>We Accept</div>
                </div>
            </div>
        </div>
    )
}

export default Footer
