import React from 'react'
import { Routes, Route } from 'react-router-dom'
import Home from '../Home';
import Trending from '../components/OurCourses/Trending/Trending';
import Upcoming from '../components/OurCourses/Upcoming/Upcoming';
import Allcourses from '../components/OurCourses/ALLcourses/Allcourses';
import Blog from '../components/Navbar/SecondHeader/SecHeaderContent/Blog/Blog';
import Career from '../components/Navbar/SecondHeader/SecHeaderContent/Career/Career';
import Offers from '../components/Navbar/SecondHeader/SecHeaderContent/Offers/Offers';
import Verify from '../components/Navbar/SecondHeader/SecHeaderContent/Verify/Verify';

const HomeRouting = () => {
    return (
            <Routes>
                <Route exact path="/" element={ <Home />} />
                <Route exact path="/" element={<Trending />} />
                <Route exact path="/" element={<Upcoming />} />
                <Route path="/allcourses" element={<Allcourses />} />
                <Route path="/blog" element={<Blog />} />               
                <Route path="/career" element={<Career />} />               
                <Route path="/offers" element={<Offers />} />               
                <Route path="/verifycerficate" element={<Verify />} />               
            </Routes>
    )
}

export default HomeRouting
